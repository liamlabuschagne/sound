from playsound import playsound
import sys
from threading import Thread
import time

file = sys.argv[1]
threads = [];
for i in range(50):
	threads.append(Thread(target = playsound, args = (file,)))
for i in threads:
	i.start();
	time.sleep(0.1)
for i in threads:
	i.join();
